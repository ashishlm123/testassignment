package com.example.testassignment.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET(Api.usersApi)
    Call<ResponseBody> getUsersList();

    @GET(Api.photosAPI)
    Call<ResponseBody> getAlbumList(@Query("albumId") String albumId);
}
