package com.example.testassignment.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.testassignment.model.PhotoModel;
import com.example.testassignment.repository.AlbumRepository;

import java.util.ArrayList;

public class AlbumListingViewModel extends ViewModel {
    private MutableLiveData<ArrayList<PhotoModel>> mAlbumLists;
    private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public LiveData<ArrayList<PhotoModel>> getPhotos(String userId) {
        if (mAlbumLists == null) {
            isLoading.setValue(true);
            mAlbumLists = new MutableLiveData<>();
            AlbumRepository albumRepository = AlbumRepository.getInstance();
            mAlbumLists.setValue(albumRepository.getAlbumSet());
            albumRepository.fetchAlbums(userId, (isSuccess, userAlbumArrayList) -> {
                Log.e("albumSize", String.valueOf(userAlbumArrayList.size()));
                mAlbumLists.setValue(userAlbumArrayList);
                isLoading.setValue(false);
            });
        }
        return mAlbumLists;
    }
    public LiveData<Boolean> isLoading() {
        return isLoading;
    }
}
