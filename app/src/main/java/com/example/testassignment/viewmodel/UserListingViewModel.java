package com.example.testassignment.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.testassignment.model.UserModel;
import com.example.testassignment.repository.UserRepository;

import java.util.ArrayList;

public class UserListingViewModel extends ViewModel {
    private MutableLiveData<ArrayList<UserModel>> mUserLists;
    private final MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public LiveData<ArrayList<UserModel>> getUsers() {
        if (mUserLists == null) {
            isLoading.setValue(true);
            mUserLists = new MutableLiveData<>();
            UserRepository userRepository = UserRepository.getInstance();
            mUserLists.setValue(userRepository.getUserSet());
            userRepository.fetchUser((isSuccess, userModelArrayList) -> {
                mUserLists.setValue(userModelArrayList);
                isLoading.setValue(false);
            });
        }
        return mUserLists;
    }

    public LiveData<Boolean> isLoading() {
        return isLoading;
    }
}
