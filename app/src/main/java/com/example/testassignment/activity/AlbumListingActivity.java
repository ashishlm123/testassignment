package com.example.testassignment.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.testassignment.R;
import com.example.testassignment.adapter.AlbumsAdapter;
import com.example.testassignment.databinding.ActivityAlbumListingBinding;
import com.example.testassignment.viewmodel.AlbumListingViewModel;
import com.example.testassignment.model.PhotoModel;


public class AlbumListingActivity extends AppCompatActivity {

    AlbumsAdapter albumsAdapter;
    private AlbumListingViewModel albumListingViewModel;
    private ActivityAlbumListingBinding binding;
    String userId = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent != null) {
            userId = intent.getStringExtra("id");
        }
        binding = DataBindingUtil.setContentView(this, R.layout.activity_album_listing);
        binding.tvAlbumId.setText("Album ID: " + userId);

        albumListingViewModel = new ViewModelProvider(this).get(AlbumListingViewModel.class);

        initRecyclerView();

        albumListingViewModel.getPhotos(userId).observe(this, albumModels -> {
            albumsAdapter.notifyDataSetChanged();
        });
        albumListingViewModel.isLoading().observe(this, isLoading -> binding.progressBar.setVisibility(
                isLoading ? View.VISIBLE : View.GONE
        ));
    }

    private void initRecyclerView() {
        binding.rvAlbumLists.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        binding.rvAlbumLists.setNestedScrollingEnabled(false);
        albumsAdapter = new AlbumsAdapter(albumListingViewModel.getPhotos(userId).getValue());
        albumsAdapter.setClickListener((position, view, object) -> {
            startActivity(new Intent(getApplicationContext(), PhotoDetailsActivity.class)
                    .putExtra("photoModel", ((PhotoModel) object)));
        });
        binding.rvAlbumLists.setAdapter(albumsAdapter);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}