package com.example.testassignment.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.testassignment.R;
import com.example.testassignment.databinding.ActivityPhotoDetailsBinding;
import com.example.testassignment.databinding.ActivityUserListingBinding;
import com.example.testassignment.model.PhotoModel;
import com.squareup.picasso.Picasso;

public class PhotoDetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PhotoModel photoInfo = getIntent().getParcelableExtra("photoModel");
        ActivityPhotoDetailsBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_photo_details);
        binding.setPhoto(photoInfo);

        assert photoInfo != null;
        Picasso.get().load(photoInfo.getPhotoUrl()).into(binding.imgPhoto);
    }
}