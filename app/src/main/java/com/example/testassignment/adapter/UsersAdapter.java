package com.example.testassignment.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;


import com.example.testassignment.R;
import com.example.testassignment.RecyclerViewItemClickListener;
import com.example.testassignment.databinding.ItemUsersBinding;
import com.example.testassignment.model.UserModel;

import java.util.ArrayList;

public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {
    private final ArrayList<UserModel> userLists;
    private RecyclerViewItemClickListener clickListener;

    public UsersAdapter(ArrayList<UserModel> userLists) {
        this.userLists = userLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_users, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel userModel = userLists.get(position);
        holder.binding.tvId.setText(userModel.getId());
        holder.binding.tvName.setText(userModel.getName());
        holder.binding.tvEmail.setText(userModel.getEmail());
        holder.binding.tvPhone.setText(userModel.getPhone());
    }

    @Override
    public int getItemCount() {
        return userLists.size();
    }

    public void setClickListener(RecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemUsersBinding binding;

        private ViewHolder(ItemUsersBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                if (clickListener != null) {
                    clickListener.onRecyclerViewItemClicked(getLayoutPosition(), v, userLists.get(getLayoutPosition()));
                }
            });
        }
    }
}
