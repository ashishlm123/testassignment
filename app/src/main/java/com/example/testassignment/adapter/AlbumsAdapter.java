package com.example.testassignment.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.testassignment.R;
import com.example.testassignment.RecyclerViewItemClickListener;
import com.example.testassignment.databinding.ItemAlbumsBinding;
import com.example.testassignment.model.PhotoModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.ViewHolder> {

    private final ArrayList<PhotoModel> albumLists;
    private RecyclerViewItemClickListener clickListener;

    public AlbumsAdapter(ArrayList<PhotoModel> albumLists) {
        this.albumLists = albumLists;
    }

    @NonNull
    @Override
    public AlbumsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.item_albums, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumsAdapter.ViewHolder holder, int position) {
        PhotoModel photoModel = albumLists.get(position);
        holder.binding.tvImgText.setText(photoModel.getPhotoTitle());

        Picasso.get().load(photoModel.getThumbUrl()).into(holder.binding.ivImg);
    }

    @Override
    public int getItemCount() {
        return albumLists.size();
    }

    public void setClickListener(RecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ItemAlbumsBinding binding;

        private ViewHolder(ItemAlbumsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(v -> {
                if (clickListener != null) {
                    clickListener.onRecyclerViewItemClicked(getLayoutPosition(), v, albumLists.get(getLayoutPosition()));
                    Log.e("album_title", albumLists.get(getLayoutPosition()).getPhotoTitle());
                }
            });
        }
    }
}
