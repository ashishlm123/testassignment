package com.example.testassignment;

import android.view.View;

public interface RecyclerViewItemClickListener {
    void onRecyclerViewItemClicked(int position, View view, Object object );
}
